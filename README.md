Small script to export Sipwise (Asterisk) voicemail greetings which are helpd in the database, and write the wav files to disk.

Edit script to  correct database names and credentials

Run this script as either 'voicemail_export.php <uuid>' or 'voicemail_export.php all' 

Using a uuid (Sipwise uuid of the subscriber) will export only that users' greetings. specifying 'all' will export all users)

Output will all be under /tmp/var/spool/asterisk/voicemail/<uuid>