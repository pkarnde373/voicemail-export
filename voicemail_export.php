#!/usr/bin/php -q
<?php

/* 
 *
 * Voicemail Export script.
 *
 * Edit below with correct database names and credentials 
 *
 * Run this script as either 'voicemail_export.php <uuid>' or 'voicemail_export.php all'
 *
 * Using a uuid (Sipwise uuid of the subscriber) will export only that users' greetings. specifying 'all' will export all users
 *
 * Output will all be under /tmp/var/spool/asterisk/voicemail/<uuid>
 *
 * Use at your own risk. Modify as you wish
 *
 * */

$db_host = 'sipwise';
$db_name = 'kamailio';
$db_login = 'USER';
$db_pass = 'PASSWORD';

if($argc == 2) {
$uuid = $argv[1];
} else {
print("Usage ".$argv[0]." <uuid>\n");
print("Where uuid is the uuid of one subscriber to export, or \"all\" to export all subscribers voicemail greetings\n");
exit(0);
}
// connect to db
$con = mysqli_connect($db_host, $db_login, $db_pass) or die("Could not connect : " . mysqli_error());
mysqli_select_db($con,$db_name) or die("Could not select database $db_name");

$rows = 0;
// select recordings where msgnum = -1 which denotes a greeting
$sql="SELECT mailboxuser,dir,recording FROM voicemail_spool WHERE msgnum = -1";
if ($uuid != 'all') 
    $sql .= " AND mailboxuser = '$uuid'";

if(!($result = mysqli_query($con,$sql))) {
print("Invalid query: " . mysqli_error($con)."\n");
print("SQL: $sql\n");
die();
}

if(mysqli_num_rows($result) > 0) { // we found some recordings
    while ($row = mysqli_fetch_assoc($result)) {

        $path_parts = pathinfo($row['dir'] . '.wav');
        $dir = $path_parts['dirname'];
        $filename = $path_parts['basename'];
        $outdir = '/tmp' . $dir;
        if (!file_exists($outdir)) mkdir($outdir,0755,true) || die("Could not create directory $outdir");
        $fp = fopen("$outdir/$filename",'w');
        fwrite($fp,$row['recording']);
        fclose($fp);
        $rows++;
    }
}
print("$rows greetings exported. you will find them under /tmp/var/spool/asterisk/voicemail/\n");

?>
